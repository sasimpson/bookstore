package api

import (
	"net/http"

	"github.com/gorilla/mux"
)

//BookHandler will define how the implemenations should handle /books
type BookHandler interface {
	All() http.Handler
	View() http.Handler
	Add() http.Handler
	Router() *mux.Router
	Start()
}
