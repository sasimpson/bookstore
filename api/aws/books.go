package aws

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"

	"github.com/apex/gateway"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/gorilla/mux"
	ddb "gitlab.com/sasimpson/bookstore/interfaces/dynamodb"
	"gitlab.com/sasimpson/bookstore/models"
)

var (
	isbn13Regexp = regexp.MustCompile(`[0-9]{3}\-[0-9]{10}`)
	dbSession    = dynamodb.New(session.New(), aws.NewConfig().WithRegion("us-east-1"))
)

//BookAPI is the API Implementation for AWS Services, primarily Lambda.
type BookAPI struct{}

//All - get all implementation handler
func (aws *BookAPI) All() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		dynamobook := ddb.DynamoBook{DB: dbSession}
		books, err := dynamobook.All()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(books)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	})
}

//View - get one implementation handler
func (aws *BookAPI) View() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		vars := mux.Vars(r)
		isbn := vars["isbn"]
		if !isbn13Regexp.MatchString(isbn) {
			http.Error(w, "need proper ISBN13", http.StatusBadRequest)
			return
		}

		dynamobook := ddb.DynamoBook{DB: dbSession}
		book, err := dynamobook.Get(isbn)
		if err != nil {
			if err == models.ErrBookNotFound {
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(book)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	})
}

//Add - add new book implementation handler
func (aws *BookAPI) Add() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		if r.Header.Get("content-type") != "application/json" {
			log.Printf("request headers: %#v", r.Header)
			http.Error(w, "invalid content type", http.StatusNotAcceptable)
			return
		}

		book := new(models.Book)
		err := json.NewDecoder(r.Body).Decode(&book)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		dynamobook := ddb.DynamoBook{DB: dbSession}
		err = dynamobook.Add(book)
		if err != nil {
			log.Printf("error adding book: %#v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.WriteHeader(http.StatusCreated)
		return
	})
}

//Router for this function implementation
func Router() *mux.Router {
	awsAPI := BookAPI{}
	r := mux.NewRouter()
	a := r.PathPrefix("/books").Subrouter()
	a.Handle("/", awsAPI.All()).Methods("GET")
	a.Handle("/{isbn}", awsAPI.View()).Methods("GET")
	a.Handle("/", awsAPI.Add()).Methods("POST")

	return r
}

//Start - start for the function
func Start() {
	log.Fatal(gateway.ListenAndServe(":3000", Router()))
}
