module gitlab.com/sasimpson/bookstore

go 1.13

require (
	github.com/apex/gateway v1.1.1
	github.com/aws/aws-lambda-go v1.13.2
	github.com/aws/aws-sdk-go v1.25.0
	github.com/gorilla/mux v1.7.3
	github.com/pkg/errors v0.8.1 // indirect
)
