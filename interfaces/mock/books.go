package mock

import "gitlab.com/sasimpson/bookstore/models"

type bookMock struct{}

func (b *bookMock) All() (*[]models.Book, error) {
	panic("not implemented")
}

func (b *bookMock) Get(_ string) (*models.Book, error) {
	panic("not implemented")
}

func (b *bookMock) Add(_ *models.Book) error {
	panic("not implemented")
}
