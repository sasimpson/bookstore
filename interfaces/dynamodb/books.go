package dynamodb

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"gitlab.com/sasimpson/bookstore/models"
)

//DynamoBook is the data structure for dynamo based BookModel implementation
type DynamoBook struct {
	DB *dynamodb.DynamoDB
}

var booksTable = "Books"

//All implements getting all books
func (b *DynamoBook) All() (*[]models.Book, error) {
	params := &dynamodb.ScanInput{
		TableName: aws.String(booksTable),
	}

	results, err := b.DB.Scan(params)
	if err != nil {
		return nil, err
	}

	var books []models.Book

	for _, i := range results.Items {
		book := models.Book{}
		err = dynamodbattribute.UnmarshalMap(i, &book)
		if err != nil {
			return nil, err
		}

		books = append(books, book)
	}

	return &books, nil
}

//Get Implementation for DynamoDB
func (b *DynamoBook) Get(isbn string) (*models.Book, error) {

	input := &dynamodb.GetItemInput{
		TableName: aws.String(booksTable),
		Key: map[string]*dynamodb.AttributeValue{
			"ISBN": {
				S: aws.String(isbn),
			},
		},
	}

	result, err := b.DB.GetItem(input)
	if err != nil {
		return nil, err
	}

	if result.Item == nil {
		return nil, models.ErrBookNotFound
	}

	book := new(models.Book)
	err = dynamodbattribute.UnmarshalMap(result.Item, book)
	if err != nil {
		return nil, err
	}

	return book, nil
}

//Add Implementation for DynamoDB
func (b *DynamoBook) Add(book *models.Book) error {

	av, err := dynamodbattribute.MarshalMap(book)
	if err != nil {
		return err
	}
	log.Printf("av: %#v", av)

	input := &dynamodb.PutItemInput{
		TableName: aws.String(booksTable),
		Item:      av,
		// Key: map[string]*dynamodb.AttributeValue{
		// 	"ISBN": {
		// 		S: aws.String(book.ISBN),
		// 	},
		// },
	}

	_, err = b.DB.PutItem(input)
	if err != nil {
		return err
	}
	return nil
}
