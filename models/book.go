package models

import "errors"

//Book structure of data
type Book struct {
	ISBN   string `json:"isbn" dynamodbav:"ISBN"`
	Title  string `json:"title" dynamodbav:"Title"`
	Author string `json:"author" dynamodbav:"Author"`
}

//BookModel is the interface for all book implementations
type BookModel interface {
	All() (*[]Book, error)
	Get(string) (*Book, error)
	Add(*Book) error
}

var (
	//ErrBookNotFound - not found error for books
	ErrBookNotFound = errors.New("Book Not Found")
)
