package main

import (
	api "gitlab.com/sasimpson/bookstore/api/aws"
)

func main() {
	api.Start()
}
